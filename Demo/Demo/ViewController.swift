//
//  ViewController.swift
//  Demo
//
//  Created by Qihang Cheng on 2019/3/18.
//  Copyright © 2019 ncnk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("设备当前 UDID 为 \(ZNUDID.getUDID())")
        
        ZNUDID.printData()
        
    }

}

