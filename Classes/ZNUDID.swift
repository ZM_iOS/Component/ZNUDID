//
//  ZNUDID.swift
//  ZNUDID
//
//  Created by Qihang Cheng on 2018/10/30.
//  Copyright © 2018 Lucifer. All rights reserved.
//

import UIKit
import AdSupport
import Security

public class ZNUDID {
    
    private static let kZNUDIDBundleIdKey      = "ZNUDID_BundleId"
    private static let kZNUDIDCreateTimeKey    = "ZNUDID_BundleIdentifier"
    private static let kZNUDIDUserDeviceIdentifierKey  = "kZNUDIDUserDeviceIdentifierKey"
    
    private static let kZNUDIDSharedNameKey    = "com.znudid.shared"
    private static let kZNUDIDDataSaveKey      = "ZNUDID_DataSaveKey"
    
    private static var dictCache: Dictionary<String, Any>?

    // MARK: - disable init method
    private init() {
    }
    
    // MARK: - private method
    
    private class func generateKeychainQuery () -> Dictionary<String, Any> {
        var keychainQuery: Dictionary<String, Any> = Dictionary.init()
        keychainQuery.updateValue(kSecClassGenericPassword, forKey: kSecClass as String)
        keychainQuery.updateValue(kZNUDIDSharedNameKey, forKey: kSecAttrService as String)
        keychainQuery.updateValue(kZNUDIDDataSaveKey, forKey: kSecAttrAccount as String)
        keychainQuery.updateValue(kSecAttrAccessibleAfterFirstUnlock, forKey: kSecAttrAccessible as String)
        return keychainQuery
    }
    
    private class func generateFreshUDID () -> String {
        let IDFV = UIDevice.current.identifierForVendor?.uuidString
        var prefixStr = ""
        if let UDID32 = IDFV?.replacingOccurrences(of: "-", with: "") {
            var i: Int = 1
            for str in UDID32 {
                if i%4==0 {
                    prefixStr.append(str)
                }
                i+=1
            }
            prefixStr+=UDID32
        }
        return prefixStr.lowercased()
    }
    
    private class func dictFromData(_ data: Data) -> Dictionary<String, Any>? {
        do {
            if #available(iOS 12.0, *) {
                return try NSKeyedUnarchiver.unarchivedObject(ofClasses: [NSDictionary.self], from: data) as? Dictionary<String, Any>
            } else {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? Dictionary<String, Any>
            }
        } catch {
            return nil
        }
    }
    
    private class func dataFromDict(_ dict: Dictionary<String, Any>) -> Data? {
        do {
            if #available(iOS 12.0, *) {
                return try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: true)
            } else {
                return NSKeyedArchiver.archivedData(withRootObject: dict)
            }
        } catch {
            return nil
        }
    }
    
    // MARK: - read Data from cache
    
    private class func getDict () -> Dictionary<String, Any>? {
        if let dict = getDataFromKeychain() {
            return dict;
        }
        if let dict = getDataFromPasteboard() {
            return dict;
        }
        if let dict = getDataFromUserDefault() {
            return dict;
        }
        return nil
    }
    
    private class func getDataFromKeychain () -> Dictionary<String, Any>? {
        var keychainQuery = generateKeychainQuery()
        keychainQuery.updateValue(kCFBooleanTrue, forKey: kSecReturnData as String)
        keychainQuery.updateValue(kSecMatchLimitOne, forKey: kSecMatchLimit as String)
        var queryResult: AnyObject?
        let readStatus = withUnsafeMutablePointer(to: &queryResult) {
            SecItemCopyMatching(keychainQuery as CFDictionary, UnsafeMutablePointer($0))
        }
        if readStatus == errSecSuccess,
            let data = queryResult as? Data,
            let dictionary = dictFromData(data) {
                return dictionary
        }
        return nil
    }
    
    private class func getDataFromPasteboard () -> Dictionary<String, Any>? {
        let pasteboard = UIPasteboard.init(name: UIPasteboard.Name(rawValue: kZNUDIDSharedNameKey), create: false);
        if let data = pasteboard?.data(forPasteboardType: kZNUDIDDataSaveKey),
            let dictionary = dictFromData(data) {
            return dictionary
        }
        return nil
    }
    
    private class func getDataFromUserDefault () -> Dictionary<String, Any>? {
        let userDefault = UserDefaults.init(suiteName: kZNUDIDSharedNameKey)
        if let data = userDefault?.data(forKey: kZNUDIDDataSaveKey),
            let dictionary = dictFromData(data) {
            return dictionary
        }
        return nil
    }
    
    // MARK: - save Data to cache
    
    private class func saveDict (dict: Dictionary<String, Any>) {
        guard let data = dataFromDict(dict) else {
            return
        }
        saveDataToKeychain(data: data)
        saveDataToPasteboard(data: data)
        saveDataToUserDefault(data: data)
    }
    
    private class func saveDataToKeychain (data: Data) {
        var keychainQuery = generateKeychainQuery()
        SecItemDelete(keychainQuery as CFDictionary)
        keychainQuery.updateValue(data, forKey: kSecValueData as String)
        SecItemAdd(keychainQuery as CFDictionary, nil)
    }
    
    private class func saveDataToPasteboard (data: Data) {
        let pasteboard = UIPasteboard.init(name: UIPasteboard.Name(rawValue: kZNUDIDSharedNameKey), create: true);
        pasteboard?.setData(data as Data, forPasteboardType: kZNUDIDDataSaveKey)
    }
    
    private class func saveDataToUserDefault (data: Data) {
        let userDefault = UserDefaults.init(suiteName: kZNUDIDSharedNameKey)
        userDefault?.set(data, forKey: kZNUDIDDataSaveKey)
        userDefault?.synchronize()
    }
    
    // MARK: - public method
    
    open class func getUDID () -> String {
        if let UDIDString = dictCache?[kZNUDIDUserDeviceIdentifierKey] as? String {
            return UDIDString
        }
        if let dict = getDict(), let UDIDString = dict[kZNUDIDUserDeviceIdentifierKey] as? String {
            if dictCache == nil {
                dictCache = dict
                saveDict(dict: dict)
            }
            return UDIDString
        }
        let UDIDString = generateFreshUDID()
        let timestamp = Int(Date().timeIntervalSince1970*1000)
        let dict: Dictionary<String, Any> = [
            kZNUDIDBundleIdKey : Bundle.main.bundleIdentifier ?? "",
            kZNUDIDCreateTimeKey : timestamp,
            kZNUDIDUserDeviceIdentifierKey : UDIDString
        ]
        dictCache = dict
        saveDict(dict: dict)
        return UDIDString
    }
    
    // MARK: - test case
    
    class func printData() {
        debugPrint("\n----------")
        debugPrint("读取 Keychain 数据 ...")
        if let dict = getDataFromKeychain() {
            debugPrint("bundleID = \(dict[kZNUDIDBundleIdKey] as? String ?? "")")
            debugPrint("timestamp = \(dict[kZNUDIDCreateTimeKey] as? Int ?? 0)")
            debugPrint("UDID = \(dict[kZNUDIDUserDeviceIdentifierKey] as? String ?? "")")
        }
        else {
            debugPrint("无数据")
        }
        
        debugPrint("\n----------")
        debugPrint("读取 Pasteboard 数据 ...")
        if let dict = getDataFromPasteboard() {
            debugPrint("bundleID = \(dict[kZNUDIDBundleIdKey] as? String ?? "")")
            debugPrint("timestamp = \(dict[kZNUDIDCreateTimeKey] as? Int ?? 0)")
            debugPrint("UDID = \(dict[kZNUDIDUserDeviceIdentifierKey] as? String ?? "")")
        }
        else {
            debugPrint("无数据")
        }
        
        debugPrint("\n----------")
        debugPrint("读取 UserDefault 数据 ...")
        if let dict = getDataFromUserDefault() {
            debugPrint("bundleID = \(dict[kZNUDIDBundleIdKey] as? String ?? "")")
            debugPrint("timestamp = \(dict[kZNUDIDCreateTimeKey] as? Int ?? 0)")
            debugPrint("UDID = \(dict[kZNUDIDUserDeviceIdentifierKey] as? String ?? "")")
        }
        else {
            debugPrint("无数据")
        }
        
        debugPrint("\n----------")
    }
    
}


